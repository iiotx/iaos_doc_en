.. IAOS Development Manual documentation master file, created by
   sphinx-quickstart on Mon Apr 15 08:36:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IAOS Development Manual's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
